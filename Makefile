CLUSTER_NAME=test-init

cluster:
	kind create cluster --name ${CLUSTER_NAME} --config ./kind.config.yaml

uncluster:
	kind delete cluster --name ${CLUSTER_NAME}

cluster_use:
	kubectl config use-context kind-${CLUSTER_NAME}

images: cluster_use
	docker build --target=source --tag tmpnext:source .
	kind load docker-image --name ${CLUSTER_NAME} tmpnext:source
	docker build --target=builder --tag tmpnext:builder .
	kind load docker-image --name ${CLUSTER_NAME} tmpnext:builder
	docker build --target=container --tag tmpnext:container .
	kind load docker-image --name ${CLUSTER_NAME} tmpnext:container
	docker build --target=container --tag tmpnext:container2 .
	kind load docker-image --name ${CLUSTER_NAME} tmpnext:container2
	docker build --target=production --tag tmpnext:production .
	kind load docker-image --name ${CLUSTER_NAME} tmpnext:production

deploy: images cluster_use
	@kubectl apply -f ./kubernetes/persistentvolumes.yaml
	@kubectl apply -f ./kubernetes/statefulset.yaml

undeploy: cluster_use
	-@kubectl delete -f ./kubernetes/statefulset.yaml
	-@kubectl delete -f ./kubernetes/persistentvolumes.yaml

start:
	docker build --target=production --tag tmpnext:production .
	docker run -it -p 3000:3000 tmpnext:production

stop:
	docker stop $(shell docker ps | grep tmpnext:latest | tr -s '  ' ' ' | cut -f 1 -d ' ')

