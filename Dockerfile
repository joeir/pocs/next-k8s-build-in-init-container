FROM node:12-alpine AS source
WORKDIR /app
COPY ./test-deploy/package.json ./test-deploy/yarn.lock ./
RUN yarn
COPY ./test-deploy ./
ENTRYPOINT ["npm", "build"]

FROM node:12-alpine AS builder
COPY --from=source /app /app
RUN npm build

FROM node:12-alpine AS container
WORKDIR /app
VOLUME [ \
  "/app/node_modules", \
  "/app/.next", \
  "/app/public", \
  "/app/package.json", \
]
COPY --from=source /app/package.json .
ENTRYPOINT ["/app/node_modules/.bin/next", "start"]

FROM container AS production
WORKDIR /app
COPY --from=builder /app/.next ./.next
COPY --from=builder /app/package.json ./
COPY --from=builder /app/node_modules ./node_modules
ENTRYPOINT ["/app/node_modules/.bin/next", "start"]
