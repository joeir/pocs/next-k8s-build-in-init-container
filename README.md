# Next build demo

This project demonstrates building a Next.js application in an `initContainer` and running the application in the `container`.

- - -

## Usage

### Orientation

The `.data` directory is mounted onto the k8s cluster so that `PersistentVolumes` can be provisioned. During the run, the `node_modules` directory's `.gitignore` might get deleted. Add it back if you wish to commit the other changes.

The `kubernetes` directory contains the workload manifests.

The `test-deploy` directory contains the Next.js application.

### Test running

1. Run `make start` to test run the application locally. Access it at [http://localhost:3000](http://localhost:3000)

### Setting up

1. Run `make cluster` to bring up a k8s cluster on your local machine
2. Run `make images` to build the required Docker images and push it to the k8s cluster
3. Run `make deploy` to deploy the workloads onto the k8s cluster

### Simulating an upgrade

In the `kubernetes/statefulset.yaml` manifest, search for the keyword `TODO`. These will contain blocks/properties you can swap out for each other that will simulate various maintenance operations like build failure, run failure, and upgrade

### Tearing down

1. Run `make undeploy` to bring down the workloads
2. Run `make uncluster` to bring down the k8s cluster

- - -

## Thoughts

This pattern could be useful when:

1. A Next.js application needs to be built with different environment variables for deployment in different regions
2. Some static resources are hosted remotely and need to be pulled in at build-time using access tokens to authenticate/authorize

However:

1. Would the environment variables be better stored in a remote configuration that the Next.js application reads from at boot?
